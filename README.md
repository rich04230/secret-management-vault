## [HashiCorp Vault](https://www.vaultproject.io/)

- Vault is a secrets management system allowing you to store sensitive data which is encrypted at rest. 
- It’s ideal to store sensitive configuration details such as passwords, encryption keys, API keys.
- Hashicorp offers two versions of Vault:
	- open source
	- enterprise

### Quick start
- Install and launch HashiCorp Vault

```bash
brew install vault
```

or [download Vault](https://www.vaultproject.io/downloads) for your operating system

- starts Vault in development mode using in-memory storage without transport encryption
```bash
vault server --dev --dev-root-token-id="00000000-0000-0000-0000-000000000000"
```

- starts Vault in [production mode](https://www.vaultproject.io/guides/production.html)
	- Make sure to use proper SSL certificates and a reliable storage backend for production use.

- Store configuration in Vault using the command line
```bash
export export VAULT_TOKEN="00000000-0000-0000-0000-000000000000"
export VAULT_ADDR="http://127.0.0.1:8200"
```

```bash
# store a configuration key-value pairs inside Vault:
vault kv put secret/gs-vault-config example.username=demouser example.password=demopassword
vault kv put secret/gs-vault-config/cloud example.username=clouduser example.password=cloudpassword
```

### Spring Vault
- Spring Vault provides familiar Spring abstractions and client-side support for accessing, storing and revoking secrets. 
- It offers both low-level and high-level abstractions for interacting with Vault, freeing the user from infrastructural concerns.

### Spring Cloud Vault

### Reference
- [Spring Boot with Vault](https://spring.io/guides/gs/vault-config/)
- [Spring Vault](https://spring.io/projects/spring-vault)
- [Spring Vault in practice](https://www.baeldung.com/spring-vault)
- [Vault](https://www.baeldung.com/vault)